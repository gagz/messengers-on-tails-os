# Messengers on Tails OS
This project should enable casual users of Tails to set up clients for the messenger protocols Signal, Matrix, XMPP and Telegram. It demonstrates how to set up the neccessary sofware, register new account(s) without smartphone and start messaging. Setup can be either done using step-by-step documentation or using provided shell scripts.

## [Wiki: HowTo Messenger on Tails](../../../wikis/HowTo)


## Support
Need support with troubleshooting Messengers on Tails?

- E-Mail & PGP: @about.privacy
- Matrix: `@about.privacy:systemli.org` (Fingerprint: c1zT KD8y 2M4f /kfP +VVZ iawC 0TLc CE5g pVxB od78 GVI)

## Contributing
Feel free to contribute!

## Acknowledgment/Rescources
- OUTDATED HowTo on [Installing and running Signal on Tails](https://bisco.org/notes/installing-and-running-signal-on-tails/)
- [Element auf Tails installieren](https://wiki.systemli.org/howto/matrix/element_auf_tails)
- [Tails: Configuring additional APT repositories (for advanced users)](https://tails.boum.org/doc/persistent_storage/additional_software/#index6h1)
- [signal-cli issue #614 problem connecting over Tor](https://github.com/AsamK/signal-cli/issues/614)
