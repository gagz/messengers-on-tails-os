#!/usr/bin/env bash

echo -n "State designation for the new Signal account (for local distinction when starting Signal-Desktop), e.g. associated phone no, serial number etc."
read -r name
echo "Creating executable shell script + desktop file for a more convenient launch of $name instance of Signal Desktop…"
cp /home/amnesia/Persistent/signal-desktop.sh /home/amnesia/Persistent/signal-desktop_$name.sh
cp ~/.local/share/applications/Signal.desktop ~/.local/share/applications/Signal_$name.desktop
sed -i -r "/s/org\.signal\.Signal\/org\.signal\.Signal\s--file-forwarding org\.signal\.Signal\s--user-data-dir=~\/\.var\/app\/org\.signal\.Signal\/config\/Signal_\$name\s@@u\s%U\s@@/" /home/amnesia/Persistent/signal-desktop_$name.sh
sed -i -r "/s/Name=Signal\sDesktop/Name=Signal\sDesktop\s\$name/" "/s/signal-desktop.sh/signal-desktop_\$name.sh/" ~/.local/share/applications/Signal_$name.desktop
